import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'ngx-editor';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  forma!: FormGroup;
  sexo: any[] = ['Masculino', 'Femenino'];
lista = [];


  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService,
              private _usuariosService: UsuariosService
              ) { 
    this.crearFormulario();
    //this.cargarDataAlFormulario();
    this.cargarDataAlFormulario2();

   

  }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched
  }

  get apellidoNoValido(){
    return (this.forma.get('apellido')?.invalid && !this.forma.get('apellido')?.errors?.['noZaralai']) && this.forma.get('apellido')?.touched
  }

  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched
  }

  get distritoNoValido(){
    return this.forma.get('direccion.distrito')?.invalid && this.forma.get('direccion.distrito')?.touched
  }

  get ciudadNoValid(){
    return this.forma.controls['direccion'].get('ciudad')?.invalid && this.forma.controls['direccion'].get('ciudad')?.touched;
  }

  get apellidoNoZaralai (){
    return this.forma.get('apellido')?.errors?.['noZaralai'];
  }

  get pass1NoValido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2NoValido(){
    const pass1 = this.forma.get('pass1')?.value;
    const pass2 = this.forma.get('pass2')?.value;
    return (pass1 === pass2) ? false: true;
  }

  get usuarioNoValido(){
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioExistente(){
    return this.forma.get('usuario')?.errors?.['existe']
  }

  get emailExistente(){
    return this.forma.get('email')?.errors?.['existe']
  }


  crearFormulario(): void{
    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      // apellido: ['', [Validators.required, Validators.minLength(3), Validators.pattern(/^[a-zA-ZñÑ\s]+$/), this.validadores.noZaralai]],
      correo: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario, Validators.maxLength(15)], //Validador Síncrono
      sexo:['', Validators.required],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
    }, {
      Validators: this.validadores.passwordsIguales('pass1', 'pass2')
    } as AbstractControlOptions );
  }

  cargarDataAlFormulario():void{
    // this.forma.setValue({
    //   nombre: 'Juan',
    //   apellido: 'Perez',
    //   correo: 'juan@gmail.com',
    //   direccion: {
    //     distrito: 'Central',
    //     ciudad: 'Oruro'
    //   }
    // })
  };
  cargarDataAlFormulario2():void{ //tercer forma para modificar valor
    this.forma.patchValue({
      apellido: 'LeonA'
    })
  }

  guardar(): void {
    console.log(this.forma.value);

    console.log('usuario registrado');
    
    // this._usuariosService.agregarNew(this.forma.value)
    //Reset del formulario
    this.LimpiarFormulario()
  }

  LimpiarFormulario(){
    //this.forma.reset(); //este nos limpia todos los elementos
    this.forma.reset({
      nombre: 'listo'
    }); //asi se limpia todo menos este, pero tiene que tener informacion
  }

  nombreUsuarioNoRepeat(){

  }

}
