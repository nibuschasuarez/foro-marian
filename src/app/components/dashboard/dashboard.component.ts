import { Component, OnInit } from '@angular/core';
import { Editor, Toolbar } from 'ngx-editor';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  nombre_usuario!: string;
  constructor(private _usuariosService: UsuariosService) { 
    this.LoginDatos();
  }

  ngOnInit(): void {

  }

  LoginDatos(){
   this.nombre_usuario = this._usuariosService.nombre_usuario
  //  this._usuariosService.snackbar(2)
  }

}
