import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { foroI } from 'src/app/interfaces/usuario.interface';
import { ForosService } from 'src/app/services/foros.service';


@Component({
  selector: 'app-foros',
  templateUrl: './foros.component.html',
  styleUrls: ['./foros.component.css']
})
export class ForosComponent implements OnInit {
    
  listaTemas : foroI[] = [];
  html: string = '';
  cantidad_mensajes = 0
  foro!: foroI;

  constructor(private _foroService : ForosService, 
    private router : Router) {
      this.verForo()
}

ngOnInit(): void { 
}

verForo(){
  this.listaTemas = this._foroService.obtenerLocalStorage()
  console.log('Mostro????');
  console.log(this.listaTemas);
}

verUnForo(id : number){
  console.log('entra a ver un Foro');
  console.log(id, 'este es el id');

  let encontrado = this._foroService.obtenerUnForo(id)
  console.log(encontrado);
  
  
  if(encontrado === undefined){
    this.html = ''
  } else{
    this.html = encontrado.foro;
    this.foro = encontrado
    this.router.navigate(['/dashboard/ver-foro', id])
  }
  console.log(encontrado);
  
  return this.foro
}

crearForo(number: number){
  this.router.navigate(['/dashboard/crear-foro', number])
}

}
