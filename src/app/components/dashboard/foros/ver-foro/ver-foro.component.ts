import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { comentariosI, foroI } from 'src/app/interfaces/usuario.interface';
import { ComentariosService } from 'src/app/services/comentarios.service';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { ForosComponent } from '../foros.component';

@Component({
  selector: 'app-ver-foro',
  templateUrl: './ver-foro.component.html',
  styleUrls: ['./ver-foro.component.css']
})
export class VerForoComponent implements OnInit {

  html: string = '';
  listaTemas !: foroI[];
  foro!: foroI;
  listaComentarios !: comentariosI[];
  comentario = '';
  mostrarComentarios: boolean = true;
  comentarioRes!: string;

  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ]


  constructor(private _foroSVC : ForosService,
              private _usuariosSVC : UsuariosService,
              private activateRoute: ActivatedRoute,
              private _comentarioSVC : ComentariosService,
              private router : Router,) {

    this.editor = new Editor(); //se instancia el editor

    this.activateRoute.params.subscribe(params => {
      const id : number = params['id'];
      let number: number = Number(id);
      console.log(number);
      console.log('entra a ver un Foro');

      let encontrado = this._foroSVC.obtenerUnForo(number);
      console.log(encontrado);
      
      if(encontrado === undefined){
        this.html = 'FORO NO ENCONTRADO'
      } else{
    this.foro = encontrado;
    this.html = encontrado.foro;
  }
    });

    //COMENTARIO
    this.mostrarComentarios = this.verComentarios()
  }

  ngOnInit(): void {}

  guardar(){
    // console.log(this.comentario);
    let fecha = new Date();
    // console.log(fecha);

    let hora = fecha.toLocaleTimeString()
    let dia = fecha.toLocaleDateString()
    let nombre = this._usuariosSVC.mostrarUser();
    
    if(nombre === null){
      console.log('no se pudo');
      
    } else {

      let comentid = Math.floor(Math.random() * 1000)
      console.log(comentid);

    const comentario : comentariosI = {
      comentid: comentid,
      foroid: this.foro.foroid,
      usuario: nombre,
      fecha: { dia, hora },
      comentario: this.comentario,
    }
    console.log(comentario);
    this._comentarioSVC.agregarNew(comentario)
    }

  }

  verComentarios(): boolean{
    if(JSON.parse(localStorage.getItem("Comentarios") || '') === null){
      console.log('no hay comentarios');
      return false
    } else {
      console.log('si hay comentarios');
      this.listaComentarios = JSON.parse(localStorage.getItem("Comentarios") || '')
      return true
    }
  }

}






// if(encontrado === undefined){
//   this.html = ''
// } else{
//   this.html = encontrado.foro;
//   this.router.navigate(['/dashboard/ver-foro', id])
// }
// console.log(encontrado);

// return encontrado