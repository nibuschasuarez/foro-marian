import { Injectable } from '@angular/core';
import { foroI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class ForosService {

  listaForos!: foroI[];
  listaTemas : foroI[] = [];
  constructor() { }

  agregarNew(foro: foroI){
    console.log(foro);

    if(localStorage.getItem('Foros') === null){
      this.listaForos = [];
      this.listaForos.unshift(foro);
      localStorage.setItem('Foros', JSON.stringify(this.listaForos))
    } else {
      this.listaForos = JSON.parse(localStorage.getItem('Foros') || '');
      this.listaForos.unshift(foro);
      localStorage.setItem('Foros', JSON.stringify(this.listaForos))
    }

    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }

  obtenerUnForo(id: number){
    console.log('entra al servicio', id);

    this.listaTemas = this.obtenerLocalStorage();
    console.log(this.listaTemas);

    const encontrado = this.listaTemas.find(e => e.foroid === id)
    console.log(encontrado);

    return encontrado
  }

  obtenerLocalStorage(){
    let listaForos = JSON.parse(localStorage.getItem("Foros") || '');
    console.log(listaForos);

    this.listaForos = listaForos
    // console.log(this.listaForos, 'obtener LocalStorage');
    return listaForos
  }




}