import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { UsuarioRegistradoI } from '../interfaces/usuario.interface';
import { UsuariosService } from './usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  listaRegistro = [];


  constructor(private _usuariosService: UsuariosService) { 
    
    this.listaRegistro = this._usuariosService.obtenerLocalStorage();
    console.log(this.listaRegistro);
  }

  //LOGIN 
  passwordsIguales(pass1Name: string, pass2Name: string): ValidationErrors | null {
    return (controls: AbstractControl)=>{ //el controls debe ser de este tipo para que podamos usar los controles del html
      //primero obtenemos los valores de los controles
      const pass1Control = controls.get(pass1Name)?.value;
      const pass2Control = controls.get(pass2Name)?.value;

      if(pass1Control.value === pass2Control.value){
        return controls.get(pass2Name)?.setErrors(null);
      } else {
        return controls.get(pass2Name)?.setErrors({ noEsIgual: true})
      };
    }
  }

  usuarioExistente(user: string){

    const lista: UsuarioRegistradoI[] = JSON.parse(localStorage.getItem("Citas") || '');
    console.log(lista);

    const Usuario = { usuario: 'jperez', password: '123'
  }

  }

  //REGISTRO
  existeUsuario(control: FormControl): Promise<any> | Observable<any> {

    const lista: UsuarioRegistradoI[] = JSON.parse(localStorage.getItem("Citas") || '');
    console.log(lista);
  
    return new Promise((resolve, reject) =>{
      console.log('hola'); //se imprime para ver si esta entrando
      setTimeout(() => {
        // console.log(this.listaRegistro, 'alba');
        
        if (lista.find(element => element.usuario === control.value)){
          console.log('nombre repetido');
          resolve({ existe: true});
          
        } else {
          console.log('nombre nuevo');
          resolve(null);
        }
      }, 3500)
      
    })
  }

  
  existeEmail(control: FormControl): Promise<any> | Observable<any> {

    const lista: UsuarioRegistradoI[] = JSON.parse(localStorage.getItem("Citas") || '');
    // console.log(lista);
  
    return new Promise((resolve, reject) =>{
      console.log('hola'); //se imprime para ver si esta entrando
      setTimeout(() => {
        // console.log(this.listaRegistro, 'alba');
        
        if (lista.find(element => element.email === control.value)){
          console.log('email repetido');
          resolve({ existe: true});
          
        } else {
          console.log('email nuevo');
          resolve(null);
        }
      }, 3500)
      
    })
  }


}